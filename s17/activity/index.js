/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	alert("Please provide your details.");
	function printUserDetails() {
		let firstName = prompt("Enter your First Name: ");
		let lastName = prompt("Enter your Last Name: ");
		let age = prompt("Enter your Age: ");
		let location = prompt("Enter your Location: ");

		console.log("Hello, " + firstName + lastName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}
	printUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printFavBands() {
		console.log("1. Rex Orange County");
		console.log("2. Coldplay");
		console.log("3. Eraserheads");
		console.log("4. Parokya ni Edgar");
		console.log("5. Bread");
	}

	printFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printFavMovies() {
		console.log("1. Forrest Gump");
		console.log("Rotten Tomatoes Rating: 95%");
		console.log("2. Avengers - End Game");
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("3. Coach Carter");
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("4. Now you see me");
		console.log("Rotten Tomatoes Rating: 70%");
		console.log("Taken");
		console.log("Rotten Tomatoes Rating: 85%");
	}

	printFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();