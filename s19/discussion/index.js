// [SELECTION] Selection Control Structures
	/*
		-it sorts out whether the statement/s are to be executed based on the condition whether it is true or false.
		   -two-way(true or false)
		   -multi-way selection
	*/

	// If ... else statement

	/*
		Syntax:
			if(condition) {
				//statement
			} else {
				//statement
			}
	*/


	let numA = -1;

	if(numA < 0) {
		console.log("The number is less than 0")
	}

	console.log(numA < 0);

	let city = "New York";

	if( city === "New York") {
		console.log("Welcome to New York City!");
	}

	// Else if
		/*
		 	- executes a statement if the previous condition/s are false  and the specified condition is true.
		 	- the "else if" clause is optional and can be added to capture additionals conditions to change the flow of the programs.
		*/

	let numB = 1;

	if(numA > 0) {
		console.log("Hello");
	} else if (numB > 0) {
		console.log("World");
	}

	city = "Tokyo";

	if(city === "New York"){
		console.log("Welcome to New York City");
	} else if (city === "Tokyo") {
		console.log("Welcome to Tokyo!");
	}


	// Else
		// - executes a statement if all of our previous conditions are false

/*	if(numA > 0){
		console.log("Hello");
	} else if (numB === 0) {
		console.log("World");
	} else {
		console.log("Again");
	}



	let age = parseInt(prompt("Enter your age:"));

	if(age <= 18) {
		console.log("Not allowede to drink");
	} else {
		console.log("Matanda ka na! Shot puno!");
	}
*/


	/*
	Mini Activty
		-create a function that will receive any value of height as an argument when you invoke it.
		-then create conditional statements:
			- if height is less than or equal to 150, print "Did not pass the min height requirement." in the console.
			- but if the height is greater than 150, print "Passed the min height requirement." in the console
	*/


	/*function height(h){
		if(h <= 150){
			console.log("Did not pass the min height requirement.");
		}
		else if(h > 150){
			console.log("Passed the min height requirement.");
		}
	}

	let h = height(parseInt(prompt("Enter your Height:")));*/
	

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return "Not a typhoon"
	} else if(windSpeed <= 61) {
		return "Tropical depression detected"
	} else if(windSpeed >= 62 && windSpeed <= 88) {
		return "Tropical storm detected"
	} else if(windSpeed >= 89 && windSpeed <=177) {
		return "Severe tropical storm detected"
	} else {
		return "Typhoon detected"
	}
}

message = determineTyphoonIntensity(parseInt(prompt("What is the windspeed")));
console.log(message);
console.warn(message);

// Truthy and Falsy

/*
	In JS a truthy value is a value that is considered true when encountered in a boolean context.

	Falsy value:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/	

// Truthy Examples:

/*let word = "true"
if(word){
	console.log("Truthy");
};

if(true) {
	console.log("Truthy");
};

if(1) {
	console.log("Truthy");
};


// Falsy Examples:
if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(undefined) {
	console.log("Falsy");
};

if(null) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
};

if(NaN) {
	console.log("Falsy");
  };*/


// Condition (Ternary) Operator - for short codes

/*
	Ternary operator takes in three operands
		1. condition
		2. expression to execute if the condition is true/truthy.
		3. expression to execute if the condition is false/falsy.

	Syntax:
		(condition) ? ifTrue_expression : isFalse_expression.


*/

// Single statement execution

/*let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False";

console.log("result of the ternary operator: " + ternaryResult);*/

// Multiple statement excution

/*let name;
function isOfLegalAge() {
	name = "John";
	return "You are of the legal age limit";
}
function isUnderAge() {
	name = "Jane";
	return "You are under the age limit";
}
let yourAge = parseInt(prompt("What is your age?"));
console.log(yourAge);

let legalAge = (yourAge > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of ternary operator in function: " + legalAge + " , " + name);*/

// [Section] Switch Statement

/*
	Can be used as an alternative to if ... else statements where the data to be used in the condition is of an expected input.

	Syntax:
		switch (expression/condition) {
			case <value>:
				statement;
				break;
			default:
				statement;
				break;
		}
*/


let day = prompt("What day of the week is it today?").toLowerCase();

console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is Red.");
		break;
	case 'tuesday':
		console.log("The color of the day is Orange.");
		break;
	case 'wednesday':
		console.log("The color of the day is Yellow.");
		break;
	case 'thursday':
		console.log("The color of the day is Green.");
		break;
	case 'friday':
		console.log("The color of the day is Blue.");
		break;
	case 'saturday':
		console.log("The color of the day is Indigo.");
		break;
	case 'sunday':
		console.log("The color of the day is Violet.");
		break;
	default:
		console.log("Please input a valid day of the week");
		break;
}

// break; - is for the ending the loop.
// switch - is not a very popular statement in coding.

// [Section] Try-Catch-Finally Statement

/*
	- try-catch is commonly used for error handling
	- will still function even if the statement is not complete/without the finally code block.
*/

function showIntensityAlert(windSpeed) {
	try{
		alerto(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error);
		console.log(error);
		console.warn(error.message);
	}
	finally {
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(56);

let error = 0;
console.log(error);