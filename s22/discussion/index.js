// console.log("test string");

// Array Methods

// Mutator methods:

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log("Current array: ");
console.log(fruits);
// push()
// Adds an element in the end of an array AND return the array`s length
// Syntax: arrayName.push()
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method. ");
console.log(fruits)

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method. ");
console.log(fruits);

// pop()
/*
	-Removes the last element in an array AND returns the removed element
	Syntax: arrayName.pop()
*/
let removedFruit = fruits.pop();
console.log(removedFruit); // Guava
console.log("Mutated array from push method. ");
console.log(fruits);

// unshift
/*
	- Adds one or more elements at the beginning of an array
	Syntax: 
		arrayName.unshift('elementA')
		arrayName.unshift('elementA', 'elementB')
*/
fruits.unshift('Lime', 'Banana');
console.log("Mutated arra from unshift method. ");
console.log(fruits);

// shift
/*
	- Removes an element at the beginning of an array AND returns the removed element
	Syntax: 
		arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method. ");
console.log(fruits);


// splice()
/*
	-Simultaneously removes elements from a specified index and adds elements
	Syntax: arrayName.splice(startingIndex, deleteCount)
*/
fruits.splice(2, 3, "Calamansi", "Lime", "Cherry");
console.log("Mutated array from splice method. ");
console.log(fruits);

// If the deleteCount and number of elements to add does not match it will still work
/*fruits.splice(2, 3, "Calamansi", "Lime", "Cherry");
console.log("Mutated array from splice method. ");
console.log(fruits);*/

// sort()
/*
    -Rearranges the array elements in alphanumeric order
    Syntax: arrayName.sort()
*/
fruits.sort();
console.log("Mutated array from sort method.");
console.log(fruits);

// reverse()
/*
    -Rearranges the array elements in descending order
    Syntax : arrayName.reverse()
*/
fruits.reverse();
console.log("Mutated array from reverse method.");
console.log(fruits);

// Non-Mutator Methods
/*
	Non-mutator methods are functions that do not modify or change an array after they`re created

	these medthods do not manipulate the original array performing various tasks such as returning elements from an array and combining array
*/
let countries = ["US", "PH", "CAN", "SG", "PH", "FR", "GE", "CH", "KR"];

// indexOf('element')
/*
	Returns the index number of the first matching element found in an array. 
	If no match was found, the result will be -1
	The search process will be done from first element proceeding to the last element.
	Syntax:
		-arrayName.indexOf(searchValue)
		-arrayName.indexOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log("Result of indexOf method: " + invalidCountry);

// lastIndexOf
/*
	- Returns the index number of the last matching element found in array
	- The search process will be done from last element proceeding to first element
	Syntax:
		-arrayName.lastIndexOf(searchValue)
		-arrayName.lastIndexOf(searchValue, fromIndex)
*/
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 3);
console.log("Result of lastIndexOf method: " + lastIndexStart);

// slice()
/*
	- Portion/Slices elements from an array AND returns a new array
	Syntax:
		arrayname.slice(startingIndex)
		arrayname.slice(startingIndex, endingIndex)
*/

let slicedArrayA = countries.slice(2);
console.log("Result from slice method: ");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method: ");
console.log(slicedArrayB);

let slicdeArrayC = countries.slice(-3);
console.log("Result from slice method: ");
console.log(slicdeArrayC);

toString();
// Returns an array as string separated by commas
let stringArray = countries.toString();
console.log(stringArray);
// console.log(stringArray[1]);

// concat
// Combines 2 arrays and returns the combined result
let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breath sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method: ');
console.log(tasks);

let tasks2 = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat method: ');
console.log(tasks2);

let tasks3 = tasksArrayA.concat("smell express", "throw react");
console.log('Result from concat method: ');
console.log(tasks3);

// join()
// Returns an array as string separated by specified separator string
let users = ['John', 'Juan', 'Dudong'];
console.log(users.join(' - '));

/*
        - Iteration methods are loops designed to perform repetitive tasks on arrays
        - Iteration methods loops over all items in an array.
        - Useful for manipulating array data resulting in complex tasks
        - Array iteration methods normally work with a function supplied as an argument
        - How these function works is by performing tasks that are pre-defined within an array's method.
*/
/*
        - Similar to a for loop that iterates on each array element.
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
        - Variable names for arrays are normally written in the plural form of the data stored in an array
        - It's common practice to use the singular form of the array content for parameter na
-------------
// forEach()
    /*
        - Similar to a for loop that iterates on each array element.
        - For each item in the array, the anonymous function passed in the forEach() method will be run.
        - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
        - Variable names for arrays are normally written in the plural form of the data stored in an array
- It's common practice to use the singular form of the array content for parameter names used in array loops
        - forEach() does not return anything.
        - Syntax
            arrayName.forEach(function(indivElement) {
                statement
            })
*/
tasks2.forEach(function(per_task){
	console.log(per_task);
})

filteredTasks = [];
tasks2.forEach(function(tasks){
	if(tasks.length > 10){
		filteredTasks.push(tasks)
	}
})

console.log(filteredTasks);

// map()
/* 
        - Iterates on each element AND returns new array with different values depending on the result of the function's operation
        - This is useful for performing tasks where mutating/changing the elements are required
        - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
        - Syntax
            let/const resultArray = arrayName.map(function(indivElement))
*/
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number + 2;
});
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);

// map() vs forEach()
let numberForEach = numbers.forEach(function(number){
	return number * number;
});

console.log(numberForEach); //undefined

//forEach(), loops over all items in the array as does map(), but forEach() does not return a new array.

// every
// checks if all statement in an array meet the fiven condition
let allValid = numbers.every(function(number){
	return number < 2;
})
console.log(allValid); // False
// some
// checks if some statement in an array meet the fiven condition
let someValid = numbers.some(function(number){
	return number < 3;
})
console.log(someValid); // True

// filter()
// Returns new array that contains elements which meets the given condition
let filterValid = numbers.filter(function(number){
	return(number < 2);
})
console.log(filterValid);

// includes()
// includes method checks if the argument passed can be found in the array
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");
console.log(productFound1) // true

let productFound2 = products.includes("Headset");
console.log(productFound2) // true

/*
        - Methods can be "chained" using them one after another
        - The result of the first method is used on the second method until all "chained" methods have been resolved
        - How chaining resolves in our example:
            1. The "product" element will be converted into all lowercase letters
            2. The resulting lowercased string is used in the "includes" method
    */
    let filteredProducts = products.filter(function(product){
        return product.toLowerCase().includes('a');
   });

    console.log(filteredProducts);

    //reduce()
    /*
		- evaluates elements from left to right and returns the array into a single value
		Syntax:

    */

 /*   - The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
        - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elemen
----
 - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
===================
- The "accumulator" parameter in the function stores the result for every iteration of the loop
        - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
- How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on*/

    let iteration = 0
    let reducedArray = numbers.reduce(function(acc, cur){
    	console.warn('current iteration: ' + ++iteration);
    	console.log('accumulator: ' + acc);
    	console.log('current value: ' + cur);

    	return acc + cur
    })

    console.log(reducedArray);

    // reducing string arrays
    let list = ['Hello', 'Again', 'World'];

    let reducedJoin = list.reduce(function(x, y) {
        return x + ' ' + y;
    });
    console.log("Result of reduce method: " + reducedJoin);