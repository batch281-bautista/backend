// console.log("test string");

// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

let trainer = {
	name: 'Ash Ketchup',
	age: 10,
	friends: {hoenn: ['May', 'Max'], kanto: ['Brock', 'Misty']},
	pokemon: ['Pikachu', 'Geodude', 'Mewtwo', 'Rayquaza', 'Groudon'],
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);
console.log("Result of dot notation: ");
console.log(trainer.name);

function Pokemon(name, level){
	this.name = name;
	this.level =  level;
	this.health = 12 * level;
	this.attack = (((5*level)/this.health)*100);
	this.thunderbolt = level + (((10*level)/this.health)*100);
	this.earthquake = level + (((15*level)/this.health)*100);
	this.psystrike = level + ((60/this.health)*100);
	this.dragonascent = level + ((70/this.health)*100);
	this.fissure = this.health;

	this.tackle = function(target){
		target.health = target.health - this.attack;
		if (target.health > 0){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + " health is now reduced to " + target.health);
		} else if(target.health < 0){
			console.log(this.name + " fainted.");
		}
	}
	this.pikachuSkill = function(target){
		target.health = target.health - this.thunderbolt;
		console.log(this.name + " used Thunderbolt to " + target.name);
		if(target.name === 'Geodude'){
			target.health = target.health + this.thunderbolt;
			console.log('Ground pokemon is immune to Electric type moves.')
			
		}

		else if (target.health > 0){
		console.log(target.name + " health is now reduced to " + target.health);
		} 
		 else if(target.health < 0){
			console.log(this.name + " fainted.");
		}
	}
	this.geodudeSkill = function(target){
		target.health = target.health - this.earthquake;
		console.log(this.name + " used Earthquake to " + target.name);
		if(target.name === 'Rayquaza'){
			console.log('Flying type pokemon is immune to Ground type Moves');
		}
		else if (target.health > 0){
		console.log(target.name + " health is now reduced to " + target.health);
		} else if(target.health < 0){
			console.log(this.name + " fainted.");
		}
	}
	this.mewtwoSkill = function(target){
		target.health = target.health - this.psystrike;
		console.log(this.name + " used Psystrike to " + target.name);
		if (target.health > 0){
		console.log(target.name + " health is now reduced to " + target.health);
		} else if(target.health < 0){
			console.log(this.name + " fainted.");
		}
	}
	this.rayquazaSkill = function(target){
		target.health = target.health - (this.dragonascent * 5);

		console.log(this.name + " used Dragonascent to " + target.name);
		if (target.name === 'Pikachu'){
		console.log(target.name + " health is now reduced to " + target.health);
		console.log('It`s super effective!');

		}else if (target.health > 0){
			console.log(target.name + " health is now reduced to " + target.health);
		}
		 else if(target.health < 0){
			console.log(this.name + " fainted.");
		}
	}
	this.groudonSkill = function(target){
		target.health = target.health - this.fissure;
		console.log(this.name + " used fissure to " + target.name);
		if (target.health > 0){
			console.log(target.name + " health is now reduced to " + target.health);
		} else if(target.health < 0){
			
			console.log(this.name + " 1-Hit KO.");
			console.log(target.name + " fainted!");
		}
	}
};

let Pikachu = new Pokemon("Pikachu", 95);
let Geodude = new Pokemon("Geodude", 64);
let Mewtwo = new Pokemon("Mewtwo", 80);
let Rayquaza = new Pokemon("Rayquaza", 90);
let Groudon = new Pokemon("Groudon", 100);

console.log('Result from square bracket notation: ');
	let pokemon = [Pikachu['name'], Geodude['name'], Mewtwo['name'], Rayquaza['name'], Groudon['name']]
console.log(pokemon);

console.log("Result of talk method");
trainer.talk();

console.log(Pikachu);
console.log(Geodude);
console.log(Mewtwo);
console.log(Rayquaza);
console.log(Groudon);
Pikachu.tackle(Geodude);
console.log(Geodude);
Pikachu.pikachuSkill(Mewtwo);
console.log(Mewtwo);
Pikachu.pikachuSkill(Geodude);
console.log(Geodude);
Geodude.geodudeSkill(Rayquaza);
console.log(Rayquaza);
Geodude.geodudeSkill(Pikachu);
console.log	(Pikachu);
Mewtwo.mewtwoSkill(Rayquaza);
console.log(Rayquaza);
Rayquaza.rayquazaSkill(Pikachu);
console.log(Pikachu);
Groudon.groudonSkill(Rayquaza);
console.log(Rayquaza);







//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}