// console.log("test string");

// Objects - a data type that is used to represent real world objects
/*
	Syntax:
		let objectName = {
			propertyA: valueA,
			propertyB: valueB,
			propertyC: valueC,
			propertyD: valueD
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
	Syntax:
		function objectName(propertA, propertyB){
			this.propertyA = propertyA,
			this.propertyB = propertB
		}
*/
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// Create new instances of the laptop object
let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating objects using object constructors: ');
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020);
console.log('Result from creating objects using object constructors: ');
console.log(myLaptop);

// Accessing object properties
// Using the dot notationg

console.log('Result from dot notation: ' + myLaptop.name);

// Using the square bracket notation
console.log('Result from square bracket notation: ' + myLaptop['name']);

// Accessing array objects

let array = [laptop, myLaptop];

// Accessing object properties in array using square bracket notation
console.log(array[0]['name']);

// Accessing object properties in array using dot notation
console.log(array[0].name);

// Initializing/Adding/Deleting/Reassigning object properties

let car = {};

// Initializing/Adding object properties using dot notation
car.name = 'Honda Civic';
car.name2 = 'Suzuki Ertiga';
console.log('Result from adding properties using dot notation: ');
console.log(car);

// Initializing/Adding object properties using square bracket notation
car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log('Result form adding properties using square bracket notation');
console.log(car);

// Deleting object properties
delete car['manufacture date'];
console.log('Result from deleting properties: ');
console.log(car);

// Reassigning values to object properties
car.name2 = "Dodge Charger R/T";
console.log('Result from deleting properties: ');
console.log(car);

// Object Methods

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name);
	}
}

console.log(person);
console.log('Result from object methods: ');
person.talk();

// Adding methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.');
}
person.walk();

// Real world application of objects

let myPokemon = {
	name: "Pikachu",
	level: 3,
	Health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon`s health is now reduce to _targetPokemonHealth_");
	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);

// Creating an object constructor
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.Health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon`s health is now reduced to _targetPokemonHealth_");
	}
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}

// Create new instances of the "Pokemon" object
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.tackle(pikachu);
rattata.faint();