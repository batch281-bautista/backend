// JSON Object
/*
	- JSON stands for JavaScript Object Notation
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// JSON as Objects
/*{
	"city": "Quezon city",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Arrays
/*"cities": [
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati city", "province": "Metro Manila", "country": "Philippines"}
]*/

// Mini Activity - Create a JSON Array that will hold atleast three breedss of dogs with properties: name, age, breed.

/*"dogs": [
	{"name": "Rirish", "age": "2", "breed": "aspin"}
]*/

// JSON Methods

// Convert Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X' }, { batchName: 'Batch Y' }];

// the "stringify" method is used to convert JavaScript Objects into a string
/*console.log('Resul from stringify method');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);

// Using Stringify method with variables
// User details
let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);*/

// Mini Activity - Create a JSON data that will accept user car details with variables brand, type, year.

/*let car = {
	brand: prompt('What brand is your car?'),
	type: prompt('What is your car model?'),
	year: prompt('What is your car year model?')
}

let carInfo = JSON.stringify({
	car: car
});

console.log(carInfo);*/

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));