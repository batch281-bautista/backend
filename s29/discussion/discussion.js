// [Section] Comparison Query Operators

// $gt/gte
/*
	- Allows us to find documents that have field number values greater than or equal to a specified field
	- Syntax:
	db.collectionName.find(field: {$gt: value});
	db.collectionName.find(field: $gte: value});
*/

db.users.find({ age: { $gt: 50 }});
db.users.find({ age: { $gte: 21 }});

// $lt/$lte
/*
	- Allows us to find documents that have field number values less than or equal to a specified field
	- Syntax:
	db.collectionName.find(field: {$lt: value});
	db.collectionName.find(field: {$lte: value});
*/

db.users.find({ age: { $lt: 50 }});
db.users.find({ age: { $lte: 65 }});

//  $ne operator
/*
	- Allows us to find documents that have field number values not equal to a specified field
	- Syntax:
	db.collectionName.find(field: {$ne: value});
*/
db.users.find({ age: { $ne: 82 }});

// $in operator
/*
	- Allows us to find documents with specific match criteria one field using different values
	db.collectionName.find(field: {$in: value});
*/
db.users.find({lastName: {$in: ['Hawking', 'Doe']}});
db.users.find({course: {$in: ['HTML', 'React']}});

// Opposite of in is not in $nin
db.users.find({courses: {$nin: ['HTML', 'React']}});

// [Section] Logical Query Operation

// $or operator
/*
	- Allows us to find documents that match a single criteria from multiple provided search criteria
	-Syntax
		db.collectionName.find({ $or: [{ fieldA: valueB }, { fieldB: valueC }]})
*/
db.users.find({$or: [{firstName: "Neil"}, {age: 21}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 21}}]});

// $and operator
/*
	- Allows us to find documents matching criteria in a single field
*/
db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

// Field Projection
/*
	- Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response
	- When dealing with complex data structures, tehre might be instances when fields are not useful for the query that we are trying to accomplish
	- To help with readability of the values returned, we can include/exclude fields from the response
*/

// Inclusion
/*
	- Allows us to include/add specific fields only when retrieving documents
	- The value provided is 1 to denote that the field is being included
	- Syntax:
	db.collectionName.find({criteria}, {field: 1}});
*/
db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
}
);

// Exclusion
/*
	- Allows us to exclude/add specific fields only when retrieving documents
	- The value provided is 0 to denote that the field is being included
	- Syntax:
	db.collectionName.find({criteria}, {field: 0}});
*/
db.users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
}
);

// Suppresing the ID field
db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
}
);

// Returning specific fields in Embedded documents
db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
}
);

// Project Specific Array Elements in Returned Array
// The $slice operator allows us to retrieve only 1 element that matches the search criteria
db.users.find(
	{ "namearr": 
		{ 
			namea: "juan" 
		} 
	}, 
	{ namearr: 
		{ $slice: 1 } 
	}
)
db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

// [Section] Evaluation Query Operators

// $regex
/*
	- Allows us to find documents that match a specific string pattern using regular expressions
	- Syntax:
	db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}})
*/

// Case sensitive query
db.users.find({firstName: {$regex: 'N'}});
db.users.find({firstName: {$regex: 'n'}});
db.users.find({firstName: {$regex: 'Steph'}});

// Case insensitive query
db.user.find({firstName:{$regex: 'j', $options:'i'}})