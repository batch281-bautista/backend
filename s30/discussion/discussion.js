// Aggregation

// Create documents for fruits database
db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["Philippines", "US"]
	},
	{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id": 2,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["US", "China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}
]);

// MongoDB Aggregation

/*
	- Used to generate manipulate data and perform operations to create filtered results

	Using the aggregate methods

	- "$match" - used to pass documents that meet specified conditions
	- Syntax
		- { $match: { field: value } }

	- $group - used to group elements together and field-value pairs using the data from the grouped elements
	- Syntax
		db.collectionName.aggregate([
			{ $match: { field: value } },
			{ $group: { _id: "$fieldB" }, { result: { operation } } }
		])
*/



db.fruits.aggregate([
 	{ $match: { "onSale": true }},
 	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);

// Field Projection with aggregation
/*
	$project - used when aggragating data to include/exclude fields from the returned results.
	Syntax:
		- { $project : { field: 1/0 } }
*/

db.fruits.aggregate([
	{ $match: { "onSale": true }},
 	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
 	{ $project: { _id: 0 }}
]);

// Sorting aggregated results
/*
	$sort - used to change the order of aggreagated results
	Syntax:
		{ $sort: { field: 1/-1 }}
*/

db.fruits.aggregate([
	{ $match: { "onSale": true }},
 	{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
 	{ $sort: { total: -1 }}
]);

// Aggregating results based on array fields
/*
	$unwind - deconstructs an array field from a collection/field with an array value to output a result for each element.
	Syntax:
		{ $unwind: field }
*/

db.fruits.aggregate([
 	{ $unwind: "$origin" }
])

// Displays fruit documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
	{ $unwind: "$origin" },
	{ $group: { _id: "$origin", kinds: { $sum: 1 } } }
]);


// One-to-one Relationship
// Create an id and stores it in the variable owner
var owner = ObjectId();

// Creates an "owner" document that uses the generate id
db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact: "09871236545"
});

// Change the "<owner_id" using the actual id in the previously created document
db.suppliers.insert({
	name: "ABC fruits",
	contact: "09193219878",
	owner_id: ObjectId("646b5a5e6a8dd6bc4ebd98b3")
})

// One-to-few relationship
db.suppliers.insertOne({
	name: "DEF Fruits",
	contact: "09179873212",
	addresses: [
		{ street: "123 San Jose St", city: "Manila" },
		{ street: "367 Gil Puya", city: "Makait" },	
	]
});

// One-to-many Relationship
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insertOne({
	_id: supplier,
	name: "GHI Fruits",
	contact: "09191234567",
	branches: [
		branch1
	]
});

db.branches.insertOne({
	_id: branch1,
	name: "BF Homes",
	address: "123 Arcardio Santos St.",
	city: "Paranaque",
	supplier_id: ObjectId("646b61cc6a8dd6bc4ebd98b8")
});

db.branches.insertOne({
	_id: branch2,
	name: "Rizal",
	address: "123 San Jose St.",
	city: "Manila",
	supplier_id: ObjectId("646b61cc6a8dd6bc4ebd98b8")
});
