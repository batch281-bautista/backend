
// The questions are as follows:
// - What directive is used by Node.js in loading the modules it needs?
	Answer: require('http');
// - What Node.js module contains a method for server creation?
	Answer: http
// - What is the method of the http object responsible for creating a server using Node.js?
	Answer: http.createServer(function(request, response))
// - What method of the response object allows us to set status codes and content types?
	Answer: response.writeHead(200)
// - Where will console.log() output its contents when run in Node.js?
	Answer: when server is successfully running
// - What property of the request object contains the address's endpoint?
	Answer: response.end(200)
