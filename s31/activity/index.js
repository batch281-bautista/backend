const http = require('http');

var port = 3000;

const app = http.createServer((request, response) => {
	if(request.url == '/loginpage'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the login page');
	}
	else if(request.url == '/register'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('I`m sorry the page you are looking for cannot be found.');
	}
	else{
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('I`m sorry the page you are looking for cannot be found.');
	}
});

app.listen(port);

console.log('The server is successfully running at localhost: 3000');