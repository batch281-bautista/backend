const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});


let users = [];

app.post("/signup", (req, res) => {
	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered`);
	} else {
		res.send("Please input your username and password");
	}
})

app.get("/users", (req, res) => {
	res.send(users);
})


app.delete("/delete-user", (req, res) => {
	let username = req.body.username;
	let userFound = false;
	for (let i = 0; i < users.length; i++){
		if (users[i].username === username){
			users.splice(i, 1);
			userFound = true;
			break;
			
		} else {
			res.status(200).send(`User does not exist`)
		}
	}
	if (userFound) {
		res.status(200).send(`User ${username} has been deleted successfully`);
	} else {
		res.status(400).send(`No users found`)
	}
})




if(require.main === module) {
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;