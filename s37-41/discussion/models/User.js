const mongoose = require("mongoose");
const userSchema = /*new*/ new mongoose.Schema({

	firstName : {
		type: String,
		require: [true,"Please input your first name"]
	},
	lastName : {
		type: String,
		require: [true,"Please input your last name"]
	},
	email : {
		type: String,
		require: [true,"Email is required"]
	},
	password : {
		type: String,
		require: [true,"Password is required"]
	},
	isAdmin : {
		type: Boolean,
		default: false /*false*/
	},
	mobileNo : {
		type: String,
		require: [true,"Please input your first mobile number"]
	},
	enrollments : [
		{
				courseId: {
					type: String,
					require: [true, "Course Id is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: 'Enrolled' /*no true value*/
				}
		}

	]
	});

module.exports = mongoose.model("User",userSchema);