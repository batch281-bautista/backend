const Product = require("../models/Product");

module.exports.embedProduct = (productData) => {
	let newProduct = new Product({
		productName : productData.product.productName,
		description : productData.product.description,
		price : productData.product.price,
		quantity: productData.product.quantity,
	})

	return Product.findOne({productName : newProduct.productName}).then((result) => {
		// console.log(result)
		if(result == null){
			return newProduct.save().then((product, error) => {
				if(error){
					return false;
				} else {
					return newProduct
				}
			})
		} else {
			if(result.productName == newProduct.productName){
				return false
			} else {
				return null
			}
		}
	})
}

module.exports.retrieveActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}

module.exports.deleteProduct = (reqBody) => {
	return Product.deleteOne({_id: reqBody._id}).then(result => {
		return true
	})
}

module.exports.retrieveAllProducts = () => {
	return Product.find().then(result => {
		return result;
	})
}

module.exports.retrieveSingleProduct = (reqParams) => {
	console.log(reqParams)
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveProduct = (reqParams) => {
	let archive = {
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, archive).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.enableProduct = (reqParams) => {
	let archive = {
		isActive: true
	}
	return Product.findByIdAndUpdate(reqParams.productId, archive).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}