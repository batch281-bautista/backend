const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return result;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : false
	})

	return User.findOne({email : reqBody.email}).then((result) => {
		if(result == null){
			return newUser.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return "Account has been Created"
				}
			})
		} else {
			const isUserExisted = bcrypt.compareSync(reqBody.password, result.password)
			if(isUserExisted){
				return "Account Existed"
			} else {
				return null;
			}
		}
	})
};

module.exports.deleteUser = (reqBody) => {
	return User.deleteOne({_id: reqBody._id}).then(result => {
		return true
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				if(result.isAdmin == true){
					return { adminAccess : auth.createAccessToken(result)}
				} else {
				return { customerAccess : auth.createAccessToken(result) }
				}	
			} else {
				return false;
			}
		}
	})
};

module.exports.registerAdmin = (reqBody) => {
	let newAdmin = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : true
	})
	return User.findOne({email : reqBody.email}).then((result) => {
		if(result == null){
			return newAdmin.save().then((user, error) => {
				if(error){
					return false;
				} else {
					return "Admin has been Created"
				}
			})
		} else {
			const isAdminExisted = bcrypt.compareSync(reqBody.password, result.password)
			if(isAdminExisted){
				return "Admin Existed"
			} else {
				return null;
			}
		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	})
}

module.exports.createOrder = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		console.log(user);
		user.orderedProducts.push({
				productId: data.productId,
				
		});
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})


	let isProductUpdated = await Product.findById(data.productId).then(product => {
		console.log(product)
		product.userOrders.push({userId : data.userId});
		return product.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		});
	});
	// console.log(isUserUpdated);
	// console.log(isProductUpdated);
	if(isUserUpdated && isProductUpdated) {
		return "Succesfully CheckedOut your orders Thank you so much for your patronage"
	} else {
		return "Checkout failed"
	}
}

module.exports.retrieveAllUserDetails = () => {
	return User.find({isAdmin : false}).then(result => {
		return result;
	})
}