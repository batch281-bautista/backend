const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		require: [true, "Product name is required"]
	},
	description: {
		type: String,
		require: [true, "Product description is required"]
	},
	price: {
		type: Number,
		require: [true, "Product price is required"]
	},
	quantity: {
		type: Number,
		require: [true, "Product quantity is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [{
		userId: {
			type: String,
			required: [true, "User Id is required"]
		},
		// orderId: {
		// 	type: String,
		// 	required: [true, "Order Id is required"]
		// }
	}]
})

module.exports = mongoose.model("Product",productSchema)