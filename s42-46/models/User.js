const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	email : {
		type: String,
		require: [true, "Email is required"]
	},
	firstName : {
		type: String,
		require: [true, "firstName is required"]
	},
	lastName : {
		type: String,
		require: [true, "lastName is required"]
	},
	mobileNo : {
		type: String,
		require: [true, "Mobile Number is required"]
	},
	password : {
		type: String,
		require: [true, "Password1 is required"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	orderedProducts : [
		{
			products:[{
				productId: {
					type: String,
					require: [true, "Product Id is required"]
				},
				productName: {
					type: String,
					require: [true, "Name of product is required"]
				},
				quantity: {
					type: Number,
					require: [true, "Quantity is required"]
				},
			}],
			totalAmount: {
				type: Number,
				require: [true, "Total Amount Needed"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});



module.exports = mongoose.model("User",userSchema);