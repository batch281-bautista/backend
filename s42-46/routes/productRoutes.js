const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

router.post("/embed", auth.verify, (req, res) => {
	const accessData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(accessData.isAdmin == true){
		productController.embedProduct(accessData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Admin Access Required!");	
	}
})

router.get("/activeProducts", (req, res) => {
	productController.retrieveActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/allProducts", (req, res) => {
	const accessData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(accessData.isAdmin == true){
		productController.retrieveAllProducts(accessData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Admin Access Required!");	
	}
})

router.get("/:productId", (req, res) => {
	productController.retrieveSingleProduct(req.params).then(resultFromController => res.send(resultFromController))
})

router.delete('/deleteProduct', (req, res) => {
	productController.deleteProduct(req.body).then(resultFromController => res.send(resultFromController));
})


router.patch("/:productId", (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin == true){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Need Admin access to update a Product")
	}
})

router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin == true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Need Admin access to archive a Product")
	}
})

router.patch("/:productId/enableProduct", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin == true){
		productController.enableProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Need Admin access to archive a Product")
	}
})

module.exports = router;