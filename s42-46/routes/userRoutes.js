const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/adminCreationSecretUrl", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/getProfile", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	
		userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

router.post("/checkout", (req, res) => {
	/*let customerCheckout = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		userId : req.body.userId,
		productId : req.body.productId 
	}

	if(customerCheckout.isAdmin == false){
		userController.createOrder(customerCheckout).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Failed to Checkout your Orders")
	}*/

	let data = {
		userId : req.body.userId,
		productId : req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity
	}

	userController.createOrder(data).then(resultFromController => res.send (resultFromController))
})

router.get("/usersDetails", auth.verify, (req, res) => {
	const adminRightsUserDetails = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(adminRightsUserDetails.isAdmin == true){
		userController.retrieveAllUserDetails().then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Admin Access Required!");	
	}

	// userController.retrieveAllUserDetails().then(resultFromController => res.send(resultFromController))
})

router.delete('/deleteUser', (req, res) => {
	userController.deleteUser(req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;